<?php

declare(strict_types=1);

namespace Laudis\LaravelUsers\Middleware;

use Closure;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;
use Laudis\LaravelUsers\DummyRequestHandler;
use Laudis\LaravelUsers\Models\User;
use Laudis\UserManagement\UserManager;
use LogicException;
use Psr\Http\Server\MiddlewareInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Exception\SuspiciousOperationException;

final class Authenticate
{
    private MiddlewareInterface $loginMiddleware;
    private PsrHttpFactory $requestFactory;
    private UserManager $manager;

    public function __construct(
        MiddlewareInterface $loginMiddleware,
        PsrHttpFactory $requestFactory,
        UserManager $manager
    ) {
        $this->loginMiddleware = $loginMiddleware;
        $this->requestFactory = $requestFactory;
        $this->manager = $manager;
    }

    /**
     * @throws MassAssignmentException
     * @throws InvalidArgumentException
     * @throws LogicException
     * @throws BadRequestException
     * @throws SuspiciousOperationException
     */
    public function handle(Request $request, Closure $next)
    {
        $this->loginMiddleware->process($this->requestFactory->createRequest($request), new DummyRequestHandler());
        $user = $this->manager->getLoggedInUser();
        if ($user !== null) {
            Auth::login(
                new User(
                    [
                        'id' => $user->getId(),
                        'email' => $user->getEmail(),
                        'username' => $user->getUserName(),
                        'password_hash' => $user->getPasswordHash()
                    ]
                )
            );
        }

        return $next($request);
    }
}
