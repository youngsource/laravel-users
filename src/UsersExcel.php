<?php
declare(strict_types=1);


namespace Laudis\LaravelUsers;

use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use InvalidArgumentException;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Http\Requests\ActionRequest;
use Laudis\LaravelUsers\Models\RoleModel;
use Laudis\LaravelUsers\Models\TemporaryLogin;
use Laudis\LaravelUsers\Models\User;
use LogicException;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

final class UsersExcel extends DownloadExcel
{
    /**
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
        $this->filename = 'gebuikers-' . env('APP_NAME') . (new DateTime())->format('Y-m-d') . '.xlsx';
        $this->withHeadings(['id', 'email', 'naam', 'aangemaakt op', 'laatst aangepast', 'is tijdelijke login', 'geldig tot en met', 'rollen']);
    }

    /**
     * @param Builder $query
     * @return UsersExcel
     * @noinspection PhpMissingParamTypeInspection
     */
    protected function withQuery($query): UsersExcel
    {
        /** @noinspection PhpParamsInspection */
        return parent::withQuery($query->with('manageableRoles')->with('temporaryLogins'));
    }

    /**
     * @param User $row
     * @return array
     * @throws LogicException
     */
    public function map($row): array
    {
        /** @var TemporaryLogin|null $temporaryLogin */
        $temporaryLogin = $row->getAttribute('temporaryLogins')->first();
        /** @var Collection $roles */
        $roles = $row->getAttribute('manageableRoles');
        return [
            $row->getAttribute('id'),
            $row->getAttribute('email'),
            $row->getAttribute('username'),
            $row->getAttribute('created_at'),
            $row->getAttribute('updated_at'),
            $temporaryLogin !== null,
            $temporaryLogin === null ? null : $temporaryLogin->getAttribute('valid_until'),
            $roles->implode('name', ', ')
        ];
    }
}
