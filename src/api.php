<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\Controllers\UpdatePasswordController;

Route::prefix('api')
    ->middleware('api')
    ->group(static function () {
        Route::post('/token', [AuthTokenIssueController::class, '__invoke']);
        Route::post('/reset-password', [ResetPasswordLinkController::class, '__invoke']);
        Route::post('/change-password', [ChangePasswordWithLinkController::class, '__invoke']);
        Route::post('/update-password', [UpdatePasswordController::class, '__invoke'])->middleware('auth');
    });
