<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers;

use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use InvalidArgumentException;
use Laudis\LaravelUsers\Models\User;
use DateTime;
use Exception;
use LogicException;
use function spl_object_id;

final class UserObserver
{
    /** @var DateTime[] */
    private static array $validUntilMappings = [];
    private UserOperator $operator;

    public function __construct(UserOperator $operator)
    {
        $this->operator = $operator;
    }

    /**
     * @throws Exception
     */
    public function creating(User $user): void
    {
        $validUntil = $user->getAttribute('valid_until');
        if ($validUntil !== null) {
            self::$validUntilMappings[spl_object_id($user)] = new DateTime($validUntil);
        }
        $this->operator->cleanValidUntilAttribute($user);
    }

    /**
     * @throws JsonEncodingException
     * @throws LogicException
     */
    public function retrieved(User $user): void {
        if ($user->getAttribute('valid_until') === null) {
            $model = $user->temporaryLogins()->first();
            if ($model !== null) {
                $user->setAttribute('valid_until', $model->getAttribute('valid_until'));
            }
        }
    }

    /**
     * @throws Exception
     */
    public function updating(User $user): void
    {
        $validUntil = $user->getAttribute('valid_until');
        if ($validUntil !== null) {
            self::$validUntilMappings[spl_object_id($user)] = new DateTime($validUntil);
        } else {
            $this->operator->detachTemporaryLogins($user);
        }
        $this->operator->cleanValidUntilAttribute($user);
    }

    /**
     * @throws MassAssignmentException
     * @throws InvalidArgumentException
     */
    public function created(User $user): void
    {
        $this->operator->automaticRoleAttachment($user);
        if (isset(self::$validUntilMappings[spl_object_id($user)])) {
            $this->operator->createTemporaryLogin($user, self::$validUntilMappings[spl_object_id($user)]);
            $this->cleanMapping($user);
        }
    }

    public function updated(User $user): void
    {
        if (isset(self::$validUntilMappings[spl_object_id($user)])) {
            $this->operator->updateTemporaryLogin($user, self::$validUntilMappings[spl_object_id($user)]);
            $this->cleanMapping($user);
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public function deleting(User $user): void
    {
        $this->operator->detachRoles($user);
        $this->operator->detachTemporaryLogins($user);
    }

    private function cleanMapping(User $user): void
    {
        if (isset(self::$validUntilMappings[spl_object_id($user)])) {
            unset(self::$validUntilMappings[spl_object_id($user)]);
        }
    }
}
