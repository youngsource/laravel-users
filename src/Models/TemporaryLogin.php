<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers\Models;

use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

final class TemporaryLogin extends Model
{
    /**
     * @throws MassAssignmentException
     */
    public function __construct(array $attributes = [])
    {
        $this->guarded = [];
        $this->casts = [
            'viewed_intro_message' => 'boolean',
            'valid_until' => 'datetime'
        ];
        parent::__construct($attributes);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
