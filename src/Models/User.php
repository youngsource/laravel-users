<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use InvalidArgumentException;

final class User extends Model implements Authenticatable
{
    use Notifiable;

    /**
     * @throws MassAssignmentException
     */
    public function __construct(array $attributes = [])
    {
        $this->guarded = [];
        parent::__construct($attributes);
        $this->table = 'users';
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(RoleModel::class, 'user_roles', 'user_id', 'role_id');
    }

    /**
     * @throws InvalidArgumentException
     */
    public function manageableRoles(): BelongsToMany
    {
        return $this->belongsToMany(RoleModel::class, 'user_roles', 'user_id', 'role_id')->where('name', '!=', 'practinet-client');
    }

    public function temporaryLogins(): HasMany
    {
        return $this->hasMany(TemporaryLogin::class, 'user_id', 'id');
    }

    public function getAuthIdentifierName(): string
    {
        return $this->getAttributeValue('email');
    }

    public function getAuthIdentifier(): int
    {
        return $this->getAttributeValue('id');
    }

    public function getAuthPassword(): string
    {
        return $this->getAttributeValue('password_hash');
    }

    public function getRememberToken(): string
    {
        return '';
    }

    public function setRememberToken($value): void
    {
        //
    }

    public function getRememberTokenName(): string
    {
        return '';
    }
}
