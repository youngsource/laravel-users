<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers\Models;

use App\DateRangeModel;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class RoleModel extends Model
{
    /**
     * @throws MassAssignmentException
     */
    public function __construct(array $attributes = [])
    {
        $this->guarded = [];
        $this->table = 'roles';
        parent::__construct($attributes);
    }

    public function dateRanges(): HasMany
    {
        return $this->hasMany(DateRangeModel::class, 'role_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_roles', 'role_id', 'user_id');
    }
}
