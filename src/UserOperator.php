<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers;

use DateTimeInterface;
use Exception;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Laudis\LaravelUsers\Models\RoleModel;
use Laudis\LaravelUsers\Models\TemporaryLogin;
use Laudis\LaravelUsers\Models\User;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use function optional;

final class UserOperator
{
    private string $defaultRole;
    private PasswordHasherInterface $hasher;

    public function __construct(string $defaultRole, PasswordHasherInterface $hasher)
    {
        $this->defaultRole = $defaultRole;
        $this->hasher = $hasher;
    }

    /**
     * @throws JsonEncodingException
     * @throws Exception
     */
    public function generateRandomPasswordHash(User $user): void
    {
        $password = Str::random(8);
        $user->setAttribute('password_hash', $this->hasher->hash($password));
//        Mail::to($user)->send(new RandomGeneratedPasswordMail($password, $user));
    }

    public function cleanValidUntilAttribute(User $user): void
    {
        $attributes = $user->getAttributes();
        unset($attributes['valid_until']);

        $user->setRawAttributes($attributes);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function automaticRoleAttachment(User $user): void
    {
        $role = RoleModel::query()->where('name', $this->defaultRole)->first(['id']);
        $user->roles()->sync(optional($role)->getAttribute('id'));
    }

    /**
     * @throws MassAssignmentException
     */
    public function createTemporaryLogin(User $user, DateTimeInterface $validUntil): void
    {
        $login = new TemporaryLogin([
                'viewed_intro_message' => false,
                'user_id' => $user->getAuthIdentifier(),
                'valid_until' => $validUntil
            ]
        );

        $login->save();

        $user->temporaryLogins()->save($login);
    }

    public function updateTemporaryLogin(User $user, DateTimeInterface $validUntil): void
    {
        $user->temporaryLogins()->each(static function (TemporaryLogin $login) use ($validUntil) {
            $login->setAttribute('valid_until', $validUntil);
            $login->save();
        });
    }

    /**
     * @throws MassAssignmentException
     */
    public function syncLogin(User $user, DateTimeInterface $validUntil): void
    {
        if ($user->temporaryLogins()->exists()) {
            $this->updateTemporaryLogin($user, $validUntil);
        } else {
            $this->createTemporaryLogin($user, $validUntil);
        }
    }

    public function detachTemporaryLogins(User $user): void
    {
        $ids = $user->temporaryLogins()->get(['id'])->map(static function (TemporaryLogin $login) {
            return $login->getAttribute('id');
        });

        TemporaryLogin::destroy($ids);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function detachRoles(User $user): void
    {
        $ids = $user->roles()->get()->map(static function (RoleModel $role) {
            return $role->getAttribute('id');
        });

        $user->roles()->detach($ids);
    }
}
