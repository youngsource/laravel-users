<?php

declare(strict_types=1);


namespace Laudis\LaravelUsers;


use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;
use Laudis\LaravelUsers\Middleware\Authenticate;
use Laudis\LaravelUsers\Models\User;
use Laudis\UserManagement\JWS\JWSLoader;
use Laudis\UserManagement\Middleware\AddPossibleUserMiddleware;
use Laudis\UserManagement\UserManager;
use Laudis\UserManagement\Validation\PasswordRule;
use Laudis\UserManagement\Validation\ResetTokenRule;
use Laudis\UserManagement\Validation\ValidAudienceRule;
use Nyholm\Psr7\Factory\Psr17Factory;
use PDO;
use Psr\Container\ContainerInterface;
use Rakit\Validation\Validator;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;

use function getenv;

/**
 * Class LaravelUsersProvider
 * @package Laudis\LaravelUsers
 */
final class LaravelUsersProvider extends ServiceProvider
{
    /** @noinspection PhpUnusedParameterInspection */
    public function boot(): void
    {
        $userRegister = require __DIR__ . '/../../../vendor/laudis/usermanagement/src/register.php';
        foreach ($userRegister as $key => $implementation) {
            if ($implementation instanceof \Closure) {
                $this->app->singleton($key, $implementation);
            }
        }

        $this->app->resolving(Validator::class, static function (Validator $validator, ContainerInterface $container) {
            $validator->addValidator('password', $container->get(PasswordRule::class));
            $validator->addValidator('reset-token', $container->get(ResetTokenRule::class));
            $validator->addValidator('valid-audience', $container->get(ValidAudienceRule::class));
            return $validator;
        });

        if (!$this->app->has(Authenticate::class)) {
            $this->app->singleton(
                Authenticate::class,
                static function (ContainerInterface $container) {
                    return new Authenticate(
                        $container->get(AddPossibleUserMiddleware::class),
                        $container->get(PsrHttpFactory::class),
                        $container->get(UserManager::class)
                    );
                }
            );
        }

        if (!$this->app->has(PsrHttpFactory::class)) {
            $this->app->singleton(
                PsrHttpFactory::class,
                static function () {
                    $psr17Factory = new Psr17Factory();
                    return new PsrHttpFactory($psr17Factory, $psr17Factory, $psr17Factory, $psr17Factory);
                }
            );
        }

        if (!$this->app->has(PDO::class)) {
            $this->app->singleton(
                PDO::class,
                static function () {
                    $dsn = sprintf(
                        '%s:host=%s;dbname=%s;charset=%s',
                        getenv('DB_CONNECTION'),
                        getenv('DB_HOST'),
                        getenv('DB_NAME'),
                        getenv('DB_CHARRSET')
                    );
                    return new PDO($dsn, env('DB_USERNAME'), env('DB_PASSWORD'));
                }
            );
        }

        Nova::auth(
            static function (Request $request) {
                $user = $request->user();
                if ($user instanceof User) {
                    return $user->roles()->where('name', 'admin')->exists();
                }
                return true;
            }
        );

        $this->app->extend(
            AddPossibleUserMiddleware::class,
            static function (AddPossibleUserMiddleware $m, ContainerInterface $c) {
                return new AddPossibleUserMiddleware(
                    $c->get(UserManager::class),
                    $c->get(JWSLoader::class)
                );
            }
        );

        $this->loadRoutesFrom(__DIR__ . '/api.php');
    }
}
