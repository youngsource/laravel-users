<?php
declare(strict_types=1);


namespace Laudis\LaravelUsers\Nova\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use Laudis\LaravelUsers\Models\RoleModel;

final class RoleFilter extends Filter
{
    public function __construct()
    {
        $this->name = 'Rol Account';
    }

    public function apply(Request $request, $query, $value)
    {
        if ($value === 'none') {
            return $query->whereDoesntHave('manageableRoles');
        }
        return $query->whereHas('manageableRoles', static function (Builder $query) use ($value) {
            return $query->where('roles.id', $value);
        });
    }

    public function options(Request $request)
    {
        $values = RoleModel::query()
            ->whereNotIn('name', ['practinet-client'])
            ->get()
            ->mapWithKeys(static function (RoleModel $role) {
                return [
                    $role->getAttributeValue('name') => $role->getAttributeValue('id')
                ];
            })
            ->toArray();
        $values['geen'] = 'none';
        return $values;
    }
}
