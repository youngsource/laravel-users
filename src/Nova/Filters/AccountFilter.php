<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers\Nova\Filters;

use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;


final class AccountFilter extends Filter
{
    public function __construct()
    {
        $this->name = 'Type account';
    }

    /**
     * @noinspection PhpMissingParamTypeInspection
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function apply(Request $request, $query, $value): Builder
    {
        if ($value === 'full_login') {
            return $query->whereDoesntHave('temporaryLogins');
        }
        return $query->whereHas('temporaryLogins', static function (Builder $builder) {
            return $builder->where('valid_until', '>=', new DateTime());
        });
    }

    public function options(Request $request): array
    {
        return [
            'Tijdelijk' => 'temporary_login',
            'Volwaardig' => 'full_login'
        ];
    }
}
