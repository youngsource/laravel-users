<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers\Nova\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

final class ValidityFilter extends Filter
{
    public function __construct()
    {
        $this->name = 'Geldigheidsfilter';
    }

    /**
     * @noinspection PhpMissingParamTypeInspection
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function apply(Request $request, $query, $value): Builder
    {
        return $query->whereHas('temporaryLogins', static function (Builder $query) use ($value) {
            if ($value === 'invalid') {
                return $query->where('valid_until', '<', $value);
            }

            return $query->where('valid_until', '>=', $value);
        });
    }

    public function options(Request $request): array
    {
        return [
            'Geldig' => 'valid',
            'Ongeldig' => 'invalid'
        ];
    }
}
