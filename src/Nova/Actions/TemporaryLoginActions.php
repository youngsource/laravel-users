<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers\Nova\Actions;

use Exception;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Laudis\LaravelUsers\Models\User;
use Laudis\LaravelUsers\UserOperator;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Field;

final class TemporaryLoginActions extends Action
{
    use InteractsWithQueue;
    use Queueable;

    private bool $makeTemporary;
    private UserOperator $operator;

    public function __construct(bool $makeTemporary, UserOperator $operator)
    {
        $this->makeTemporary = $makeTemporary;
        if ($this->makeTemporary) {
            $this->name = 'Maak tijdelijk account en synchroniseer geldigheidsdatum';
        }
        else {
            $this->name = 'Maak volwaardig account';
        }
        $this->operator = $operator;
    }

    /**
     * @throws MassAssignmentException
     * @throws Exception
     */
    public function handle(ActionFields $fields, Collection $models): void
    {
        if ($this->makeTemporary) {
            $models->each(function (User $user) use ($fields) {
                $this->operator->syncLogin($user, new \DateTime($fields->get('valid_until')));
            });
        }
        else {
            $models->each(function (User $user) {
                $this->operator->detachTemporaryLogins($user);
            });
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        $tbr = parent::fields();
        if ($this->makeTemporary) {
            return array_merge($tbr, [
                DateTime::make('Geldig tot en met', 'valid_until')
            ]);
        }
        return $tbr;
    }
}
