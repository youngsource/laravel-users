<?php
declare(strict_types=1);


namespace Laudis\LaravelUsers\Nova\Actions;


use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;
use Laudis\LaravelUsers\Models\RoleModel;
use Laudis\LaravelUsers\Models\User;
use LogicException;

class RoleManagementAction extends Action
{
    use InteractsWithQueue;
    use Queueable;

    private const SYNC = 'sync';
    private const REMOVE = 'remove';

    private string $mode;

    private function __construct(string $name, string $mode)
    {
        $this->name = $name;
        $this->mode = $mode;
    }

    public static function SYNC_MODE(): self
    {
        return new self('Ken rol toe', self::SYNC);
    }

    public static function REMOVE_MODE(): self
    {
        return new self('Verwijder rol', self::REMOVE);
    }

    /**
     * @param ActionFields $fields
     * @param Collection $models
     * @throws InvalidArgumentException
     */
    public function handle(ActionFields $fields, Collection $models): void
    {
        $uids = $models->map(static function (User $user) {
            return $user->getAttribute('id');
        });
        $roleId = (int) $fields->get('role');

        if ($this->mode === self::SYNC) {

            $doubleRoles = DB::table('user_roles')
                ->whereIn('user_id', $uids)
                ->where('role_id', $roleId)
                ->get(['user_id'])
                ->pluck('user_id')
                ->all();

            $uids = $uids->diff($doubleRoles);


            $input = $uids->map(static function (int $id) use($roleId) {
                return [
                    'user_id' => $id,
                    'role_id' => $roleId
                ];
            });

            DB::table('user_roles')->insert($input->toArray());
        } elseif($this->mode === self::REMOVE) {
            DB::table('user_roles')->whereIn('user_id', $uids)
                ->where('role_id', $roleId)
                ->delete();
        }
    }

    /**
     * @return array
     * @throws LogicException
     */
    public function fields(): array
    {
        return array_merge(
            parent::fields(),
            [
                Select::make('Rol', 'role')->options(
                    RoleModel::query()
                        ->whereNotIn('name', ['practinet-client'])
                        ->get()
                        ->mapWithKeys(static function (RoleModel $role) {
                        return [ $role->getAttribute('id') => $role->getAttribute('name')];
                    })
                )
            ]
        );
    }
}
