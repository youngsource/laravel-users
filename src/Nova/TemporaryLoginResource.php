<?php /** @noinspection PhpMissingParamTypeInspection */
/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace Laudis\LaravelUsers\Nova;

use Exception;
use Laravel\Nova\Resource;
use Laudis\LaravelUsers\Models\TemporaryLogin;
use Laudis\LaravelUsers\Nova\Actions\TemporaryLoginActions;
use Laudis\LaravelUsers\Nova\Filters\ValidityFilter;
use Laudis\LaravelUsers\Models\User;
use Laudis\LaravelUsers\UserOperator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Filters\Filter;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\PasswordHasher;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use Opanegro\FieldNovaPasswordShowHide\FieldNovaPasswordShowHide;
use RuntimeException;
use function resolve;

final class TemporaryLoginResource extends Resource
{
    public static string $model = User::class;

    public static $title = 'email';
    /** @noinspection PhpMissingParentCallCommonInspection */
    public static function label(): string
    {
        return 'tijdelijke logins';
    }
    /** @noinspection PhpMissingParentCallCommonInspection */
    public static function singularLabel(): string
    {
        return 'tijdelijke login';
    }

    public function fields(Request $request): array
    {
        return [
            Text::make('Naam', 'username')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Mailadres', 'email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            DateTime::make('Geldig tot en met', 'valid_until')
                ->required()
                ->onlyOnForms()
                ->sortable(),

            DateTime::make('Geldig tot en met', static function (User $user) {
                $model = $user->temporaryLogins()->first();
                if ($model !== null) {
                    return $model->getAttribute('valid_until');
                }
                return null;
            })->hideWhenCreating()->hideWhenUpdating(),

            DateTime::make('Aangemaakt op', 'created_at')->onlyOnDetail(),

            DateTime::make('Laatst aangepast op', 'updated_at')->onlyOnDetail(),

            Boolean::make('Is Geldig', static function (User $user) {
                $model = $user->temporaryLogins()->first();
                if ($model !== null) {
                    return $model->getAttribute('valid_until') > (new \DateTime());
                }
                return false;
            })
        ];
    }

    /**
     * @throws RuntimeException
     */
    public static function indexQuery(NovaRequest $request, $query): Builder
    {
        return parent::indexQuery($request, $query)->whereHas('temporaryLogins');
    }

    /**
     * @return Action[]
     * @throws Exception
     */
    public function actions(Request $request): array
    {
        $tbr = parent::actions($request);
        $operator = resolve(UserOperator::class);
        return array_merge($tbr, [
            new TemporaryLoginActions(true, $operator),
            new TemporaryLoginActions(false, $operator),
            (new DownloadExcel())
                ->withFilename('tijdelijke-logins-' . env('APP_NAME') . (new \DateTime())->format('Y-m-d') . '.xlsx')
                ->withHeadings()
        ]);
    }

    /**
     * Get a fresh instance of the model represented by the resource.
     *
     * @return User
     */
    public static function newModel(): User
    {
        /** @var User $user */
        $user = parent::newModel();
        return $user;
    }

    /**
     * @param Request $request
     * @return Filter[]
     */
    public function filters(Request $request): array
    {
        $filters = parent::filters($request);
        return array_merge($filters, [
            new ValidityFilter()
        ]);
    }
}
