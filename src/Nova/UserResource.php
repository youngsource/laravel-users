<?php /** @noinspection PhpHierarchyChecksInspection */

/** @noinspection PhpMissingFieldTypeInspection */

/** @noinspection EfferentObjectCouplingInspection */
declare(strict_types=1);

namespace Laudis\LaravelUsers\Nova;

use Illuminate\Database\Eloquent\Relations\Relation;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Resource;
use Laudis\LaravelUsers\Nova\Actions\RoleManagementAction;
use Laudis\LaravelUsers\Nova\Actions\TemporaryLoginActions;
use Laudis\LaravelUsers\Nova\Filters\AccountFilter;
use Laudis\LaravelUsers\Models\User;
use Laudis\LaravelUsers\Nova\Filters\RoleFilter;
use Laudis\LaravelUsers\UserOperator;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laudis\LaravelUsers\UsersExcel;
use Laudis\Nova\RolesResource;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\PasswordHasher;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use Opanegro\FieldNovaPasswordShowHide\FieldNovaPasswordShowHide;
use RuntimeException;

final class UserResource extends Resource
{
    public static string $model = User::class;
    public static $title = 'email';
    public static $search = [
        'email',
        'username',
        'id'
    ];

    /** @noinspection PhpMissingParentCallCommonInspection */
    public static function label(): string
    {
        return 'gebruikers';
    }

    /**
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public static function singularLabel(): string
    {
        return 'gebruiker';
    }

    /**
     * @throws RuntimeException
     * @noinspection ReturnTypeCanBeDeclaredInspection
     */
    protected static function initializeQuery(NovaRequest $request, $query, $search, $withTrashed)
    {
        return parent::initializeQuery($request, $query, $search, $withTrashed)
            ->whereHas('roles', static function ($builder) {
                return $builder->where('name', '=', env('APP_USER_ROLE'));
            });
    }


    /**
     * @return Field[]
     */
    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make('Naam', 'username')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Mailadres', 'email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            FieldNovaPasswordShowHide::make('Wachtwoord', 'password_hash')
                ->hideFromIndex()
                ->hideFromDetail()
                ->creationRules('required', 'string', 'min:6')
                ->fillUsing(static function (
                    NovaRequest $request,
                    User $model,
                    string $attribute,
                    string $requestAttribute
                ) {
                    if (!empty($request[$requestAttribute])) {
                        /** @var PasswordHasherInterface $hasher */
                        $hasher = resolve(PasswordHasherInterface::class) ?? new PasswordHasher();
                        $model->setAttribute($attribute, $hasher->hash($request[$requestAttribute]));
                    }
                }),


            DateTime::make('Aangemaakt op', 'created_at')->hideWhenUpdating()->hideWhenCreating(),

            DateTime::make('Laatst aangepast op', 'updated_at')->onlyOnDetail(),

            DateTime::make('Geldig tot en met', 'valid_until')
                ->help('Dit veld moet enkel worden ingevuld indien men een tijdelijk account wenst')
                ->nullable(true)
                ->sortable(),

            Boolean::make('Is tijdelijk account', static function (User $user) {
                return $user->temporaryLogins()->exists();
            }),

            BelongsToMany::make('Rollen', 'manageableRoles', RolesResource::class)->onlyOnDetail()
        ];
    }

    /**
     * @return Action[]
     * @throws Exception
     */
    public function actions(Request $request): array
    {
        $tbr = parent::actions($request);
        $operator = resolve(UserOperator::class);
        return array_merge($tbr, [
            new TemporaryLoginActions(true, $operator),
            new TemporaryLoginActions(false, $operator),
            RoleManagementAction::SYNC_MODE(),
            RoleManagementAction::REMOVE_MODE(),
            new UsersExcel()
        ]);
    }

    /**
     * @return array
     */
    public function filters(Request $request): array
    {
        $tbr = parent::filters($request);
        return array_merge($tbr, [
            new AccountFilter(),
            new RoleFilter()
        ]);
    }

}
