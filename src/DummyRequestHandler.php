<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;

final class DummyRequestHandler implements RequestHandlerInterface
{
    /**
     * @return ResponseInterface
     * @throws BindingResolutionException
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $factory = resolve(PsrHttpFactory::class);
        return $factory->createResponse(response()->make());
    }
}
