<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers;

use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Auth\AuthenticationException;
use InvalidArgumentException;
use function redirect;

/**
 * Class AuthenticationHandler
 * @package Laudis\LaravelUsers
 */
final class AuthenticationHandler
{
    /**
     * @return Response|RedirectResponse|JsonResponse|null
     * @throws BindingResolutionException
     * @throws InvalidArgumentException
     */
    public function render(Exception $exception)
    {
        if ($exception instanceof AuthenticationException) {
            return redirect()->to(env('APP_URL') . '/login');
        }
        return null;
    }
}
