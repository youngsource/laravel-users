<?php
declare(strict_types=1);

namespace Laudis\LaravelUsers;

use Laudis\LaravelUsers\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

final class RandomGeneratedPasswordMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    private string $password;
    private User $user;

    public function __construct(string $password, User $user)
    {
        $this->password = $password;
        $this->user = $user;
    }

    public function build(): self
    {
        return $this->view('mails.randomGeneratedPassword', [
            'password' => $this->password,
            'user' => $this->user
        ])->subject('Uw voorlopige login werd succesvolg aangemaakt!');
    }
}
